// Top-level build file where you can add configuration options common to all sub-projects/modules.

plugins {
    id("com.github.ben-manes.versions") version ("0.5-beta-6")
}

buildscript {

    val kotlin_version = "1.5.31"
    repositories {
        google()
        mavenCentral()
        maven("https://repo1.maven.org/maven2/")
    }
    dependencies {
        classpath("com.android.tools.build:gradle:7.0.2")
        classpath(kotlin("gradle-plugin", version = kotlin_version))
        classpath ("com.github.ben-manes:gradle-versions-plugin:+")

    }
    extra.apply{
        set("kotlin_version", kotlin_version)
    }
}

allprojects {
    repositories {
        google()
        mavenCentral()
        maven("https://repo1.maven.org/maven2/")
    }
//    apply{
//        plugin("com.github.benmanes.gradle.versions.VersionsPlugin")
//    }
}

tasks.register<Delete>("clean").configure {
    delete(rootProject.buildDir)
 }
