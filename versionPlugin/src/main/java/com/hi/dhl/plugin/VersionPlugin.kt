package com.hi.dhl.plugin

import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * <pre>
 *     author: dhl
 *     date  : 2020/5/29
 *     desc  : 如果出现红色警告可以忽略，不会影响项目的编译和运行
 * </pre>
 */
class VersionPlugin : Plugin<Project> {
    override fun apply(project: Project) {
    }

    companion object {
        val constraint = "androidx.constraintlayout:constraintlayout:2.1.0"
        val navfrag = "androidx.navigation:navigation-fragment:2.3.5"
        val navui = "androidx.navigation:navigation-ui:2.3.5"
        val livedata = "androidx.lifecycle:lifecycle-livedata-ktx:2.3.1"
        val viewmodel = "androidx.lifecycle:lifecycle-viewmodel-ktx:2.3.1"
        val fragmentktx = "androidx.navigation:navigation-fragment-ktx:2.3.5"
        val uiktx = "androidx.navigation:navigation-ui-ktx:2.3.5"
        val appcompat = "androidx.appcompat:appcompat:1.3.1"
        val timber = Depend.timber
        val material = "com.google.android.material:material:1.4.0"

        val glide = "com.github.bumptech.glide:glide:4.12.0"
        val glideOkhttp = "com.github.bumptech.glide:okhttp3-integration:4.12.0"
        val glideCompile = "com.github.bumptech.glide:compiler:4.12.0"

        private const val retrofitVersion = "2.9.0"
        val retrofit = "com.squareup.retrofit2:retrofit:$retrofitVersion"
        val retrofitMoshi = "com.squareup.retrofit2:converter-moshi:$retrofitVersion"

        val moshiKotlin = "com.squareup.moshi:moshi-kotlin:1.11.0"
        val moshiAdapters = "com.squareup.moshi:moshi-adapters:1.11.0"

        private const val coroutineVersion = "1.4.2"
        val coroutineAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutineVersion"
        val coroutineCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutineVersion"
        val coroutineReactive =
            "org.jetbrains.kotlinx:kotlinx-coroutines-reactive:$coroutineVersion"

        val dataStorePref = "androidx.datastore:datastore-preferences:1.0.0"
        val dataStorePrefCore = "androidx.datastore:datastore-preferences-core:1.0.0"
    }
}
