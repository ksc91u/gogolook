pluginManagement {
  repositories {
      jcenter()
      gradlePluginPortal()
  }
}
includeBuild("versionPlugin")
include(":app")
rootProject.name = "gogolook"
