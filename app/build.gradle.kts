import org.jetbrains.kotlin.config.KotlinCompilerVersion
import org.jetbrains.kotlin.kapt3.base.Kapt.kapt

val kotlin_version: String by extra

buildscript {
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath("com.github.ben-manes:gradle-versions-plugin:0.5-beta-6")
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

plugins {
    id("com.android.application")
    id("kotlin-android")
    id("com.hi.dhl.plugin") apply (true)
    kotlin("kapt")
}
apply {
    plugin("kotlin-android")
}

apply(plugin = "com.github.ben-manes.versions")

android {
    compileSdkVersion(31)

    kapt {
        useBuildCache = true
    }

    buildFeatures {
        dataBinding = true
        viewBinding = true
    }

    defaultConfig {
        applicationId = "gogolook.app"
        minSdkVersion(23)
        targetSdkVersion(31)
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    signingConfigs {
        getByName("debug") {
            storeFile = rootProject.file("keystore/debug.keystore")
            storePassword = "android"
            keyAlias = "androiddebugkey"
            keyPassword = "android"
        }
//        create("release") {
//            storeFile = rootProject.file("release.keystore")
//            storePassword = System.getenv("ANDROID_KEYSTORE_PASSWORD")
//            keyAlias = System.getenv("ANDROID_KEYSTORE_ALIAS")
//            keyPassword = System.getenv("ANDROID_KEYSTORE_PRIVATE_KEY_PASSWORD")
//        }
    }

    buildTypes {
//        named("release") {
//            isMinifyEnabled = false
//            setProguardFiles(
//                listOf(
//                    getDefaultProguardFile("proguard-android-optimize.txt"),
//                    "proguard-rules.pro"
//                )
//            )
//            signingConfig = signingConfigs.getByName("release")
//            isDebuggable = false
//            isZipAlignEnabled = true
//            isMinifyEnabled = true
//        }
        named("debug") {
            isMinifyEnabled = false
            isDebuggable = true
            signingConfig = signingConfigs.getByName("debug")
            applicationIdSuffix = ".debug"
            versionNameSuffix = "-debug"
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation(com.hi.dhl.plugin.VersionPlugin.appcompat)
    implementation(com.hi.dhl.plugin.VersionPlugin.material)
    implementation(com.hi.dhl.plugin.VersionPlugin.constraint)
    implementation(com.hi.dhl.plugin.VersionPlugin.navfrag)
    implementation(com.hi.dhl.plugin.VersionPlugin.navui)
    implementation(com.hi.dhl.plugin.VersionPlugin.uiktx)
    implementation(com.hi.dhl.plugin.VersionPlugin.fragmentktx)
    implementation(com.hi.dhl.plugin.VersionPlugin.viewmodel)
    implementation(com.hi.dhl.plugin.VersionPlugin.livedata)
    implementation(com.hi.dhl.plugin.VersionPlugin.timber)
    implementation(com.hi.dhl.plugin.VersionPlugin.dataStorePref)
    implementation(com.hi.dhl.plugin.VersionPlugin.dataStorePrefCore)

    implementation(com.hi.dhl.plugin.VersionPlugin.retrofit)
    implementation(com.hi.dhl.plugin.VersionPlugin.retrofitMoshi)
    implementation(com.hi.dhl.plugin.VersionPlugin.moshiKotlin)
    implementation(com.hi.dhl.plugin.VersionPlugin.moshiAdapters)
    implementation(com.hi.dhl.plugin.VersionPlugin.coroutineAndroid)
    implementation(com.hi.dhl.plugin.VersionPlugin.coroutineCore)
    implementation(com.hi.dhl.plugin.VersionPlugin.coroutineReactive)

    implementation(com.hi.dhl.plugin.VersionPlugin.glide)
    implementation(com.hi.dhl.plugin.VersionPlugin.glideOkhttp)
    kapt(com.hi.dhl.plugin.VersionPlugin.glideCompile)

    implementation ("androidx.lifecycle:lifecycle-extensions:2.2.0")
    implementation ("androidx.lifecycle:lifecycle-runtime-ktx:2.3.1")

    implementation("com.squareup.okhttp3:logging-interceptor:4.9.1")

    implementation(kotlin("stdlib", KotlinCompilerVersion.VERSION))
    implementation("androidx.core:core-ktx:1.6.0")

    // Koin core features
    implementation("io.insert-koin:koin-core:3.1.2")
    // Koin main features for Android (Scope,ViewModel ...)
    implementation("io.insert-koin:koin-android:3.1.2")
    // Koin Java Compatibility
    implementation("io.insert-koin:koin-android-compat:3.1.2")

    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.3")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.4.0")
    implementation("androidx.core:core-ktx:1.6.0")
    implementation("com.github.mancj:MaterialSearchBar:0.8.5")

    val paging_version = "3.0.1"

    implementation("androidx.paging:paging-runtime:$paging_version")

}
repositories {
    maven{
        setUrl("https://jitpack.io")
    }
    mavenCentral()
}