package gogolook.dto

data class ConfigDto(
    val configValue: String
)