package gogolook.dto



data class PixbayResult(
    val total: Int,
    val totalHits: Int,
    val hits: List<PixImage>
)

data class PixImage(
    val id: Long,
    val pageURL: String,
    val previewURL: String,
    val webformatURL: String,
    val userImageURL: String
)