package gogolook.dto

class HistoryDto(
    val keywords: List<String>
)