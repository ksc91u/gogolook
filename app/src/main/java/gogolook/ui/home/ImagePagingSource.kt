package gogolook.ui.home

import androidx.paging.PagingSource
import androidx.paging.PagingState
import gogolook.dto.PixImage
import gogolook.service.PixApi
import gogolook.service.adapter.NetworkResponse
import timber.log.Timber

class ImagePagingSource(
    private val pixApi: PixApi,
    private val keyword: String,
) :
    PagingSource<Long, PixImage>() {
    override fun getRefreshKey(state: PagingState<Long, PixImage>): Long? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.nextKey
        }
    }

    override suspend fun load(params: LoadParams<Long>): LoadResult<Long, PixImage> {
        val page = params.key ?: 1
        val result = pixApi.search(keyword, page)

        return when (result) {
            is NetworkResponse.Success -> {
                val isEmpty = result.body.hits.isEmpty()
                if (isEmpty && (params.key == 0L || params.key == null)) {  //First page empty
                    LoadResult.Error(result.cause ?: UnknownError())
                } else {
                    LoadResult.Page(
                        data = result.body.hits,
                        nextKey = if (isEmpty) null else page + 1,
                        prevKey = params.key
                    )
                }
            }
            else -> {
                LoadResult.Error(result.cause ?: UnknownError())
            }
        }
    }

}