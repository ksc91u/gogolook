package gogolook.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import gogolook.databinding.ItemImage2Binding
import gogolook.databinding.ItemImageBinding
import gogolook.dto.PixImage
import gogolook.dto.PixbayResult
import gogolook.service.di.GlideApp

class ImagePagingAdapter() : PagingDataAdapter<PixImage, RecyclerView.ViewHolder>(PixImageDiffCallback()) {

    private var spanCount: Int = 1
    private val MAP: List<Int> = listOf(0, 2, 1)

    fun switchSpanCount(): Int {
        spanCount = MAP[spanCount]
        notifyDataSetChanged()
        return spanCount
    }

    override fun getItemViewType(position: Int): Int {
        return spanCount
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let { item ->
            if (spanCount == 1) {
                (holder as? ImageVH)?.bind(item)
            } else {
                (holder as? ImageVH2)?.bind(item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (spanCount == 1)
            return ImageVH(
                ItemImageBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        else
            return ImageVH2(
                ItemImage2Binding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
    }

}

//class ImagePagingAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
//    private val items: MutableList<PixImage> = mutableListOf()
//    private var spanCount: Int = 1
//
//    private val MAP: List<Int> = listOf(0, 2, 1)
//    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//        items.getOrNull(position)?.let { item ->
//            if (spanCount == 1) {
//                (holder as? ImageVH)?.bind(item)
//            } else {
//                (holder as? ImageVH2)?.bind(item)
//            }
//        }
//    }
//
//    fun switchSpanCount(): Int {
//        spanCount = MAP[spanCount]
//        notifyDataSetChanged()
//        return spanCount
//    }
//
//    override fun getItemViewType(position: Int): Int {
//        return spanCount
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
//        if (spanCount == 1)
//            return ImageVH(
//                ItemImageBinding.inflate(
//                    LayoutInflater.from(parent.context),
//                    parent,
//                    false
//                )
//            )
//        else
//            return ImageVH2(
//                ItemImage2Binding.inflate(
//                    LayoutInflater.from(parent.context),
//                    parent,
//                    false
//                )
//            )
//    }
//
//    override fun getItemCount(): Int {
//        return items.size
//    }
//
//    fun update(result: PixbayResult) {
//        items.clear()
//        items.addAll(result.hits)
//        notifyDataSetChanged()
//    }
//
//}

class ImageVH(private val _binding: ItemImageBinding) : RecyclerView.ViewHolder(_binding.root) {
    fun bind(item: PixImage) {
        GlideApp.with(_binding.imageView).load(item.webformatURL).centerCrop()
            .into(_binding.imageView)
    }
}

class ImageVH2(private val _binding: ItemImage2Binding) : RecyclerView.ViewHolder(_binding.root) {
    fun bind(item: PixImage) {
        GlideApp.with(_binding.imageView).load(item.webformatURL).centerCrop()
            .into(_binding.imageView)
    }
}

class PixImageDiffCallback : DiffUtil.ItemCallback<PixImage>() {
    override fun areItemsTheSame(oldItem: PixImage, newItem: PixImage): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: PixImage, newItem: PixImage): Boolean =
        (oldItem == newItem)

}