package gogolook.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.Observer
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.transition.TransitionManager
import gogolook.MainViewModel
import gogolook.databinding.FragmentHomeBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


val Fragment.fragmentViewScope: LifecycleCoroutineScope
    get() = viewLifecycleOwner.lifecycle.coroutineScope


class HomeFragment : Fragment() {

    private val mainViewModel: MainViewModel by sharedViewModel<MainViewModel>()
    private lateinit var homeBinding: FragmentHomeBinding
    private val adapter: ImagePagingAdapter = ImagePagingAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeBinding = FragmentHomeBinding.inflate(inflater, container, false).apply {
            recyclerView.adapter = adapter
            lifecycleOwner = this@HomeFragment
        }


        mainViewModel.pageLiveData.observe(viewLifecycleOwner, {
            fragmentViewScope.launch {
                adapter.submitData(it)
            }
        })

        mainViewModel.defaultSpanCount.observe(viewLifecycleOwner, {
            if (it !== 1) {
                switchSpanCount()
            }
        })

        mainViewModel.search("green")

        lifecycleScope.launch {
            adapter.loadStateFlow.collectLatest { loadStates ->
                mainViewModel.setLoadState(loadStates)
            }
        }
        lifecycleScope.launch {
            mainViewModel.fabClickFlow.collect {
                switchSpanCount()
            }
        }

        return homeBinding.root
    }

    fun switchSpanCount() {
        val spanCount = adapter.switchSpanCount()
        homeBinding.recyclerView.post {
            TransitionManager.beginDelayedTransition(homeBinding.recyclerView)
            (homeBinding.recyclerView.layoutManager as? GridLayoutManager)?.spanCount = spanCount
        }
        mainViewModel.setSpanCount(spanCount)
    }
}