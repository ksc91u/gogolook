package gogolook

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.mancj.materialsearchbar.MaterialSearchBar
import com.mancj.materialsearchbar.adapter.SuggestionsAdapter
import gogolook.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class MainActivity : AppCompatActivity() {

    private val mainViewModel: MainViewModel by viewModel()
    private lateinit var mainViewBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mainViewBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mainViewBinding.apply {
            vm = mainViewModel
            lifecycleOwner = this@MainActivity
            context = this@MainActivity

            searchBar.setOnSearchActionListener(object : MaterialSearchBar.OnSearchActionListener {
                override fun onSearchStateChanged(enabled: Boolean) {
                }

                override fun onSearchConfirmed(text: CharSequence?) {
                    mainViewModel.search(text.toString())
                    mainViewBinding.searchBar.closeSearch()
                }

                override fun onButtonClicked(buttonCode: Int) {
                }

            })
            searchBar.setSuggestionsClickListener(object: SuggestionsAdapter.OnItemViewClickListener{
                override fun OnItemClickListener(position: Int, v: View?) {
                    mainViewModel.searchSuggestion(position)
                    mainViewBinding.searchBar.closeSearch()
                }

                override fun OnItemDeleteListener(position: Int, v: View?) {
                    mainViewModel.removeSuggestion(position)
                }

            })
            setSupportActionBar(toolbar)
        }

        mainViewModel.historyListLiveData.observe(this, {
            mainViewBinding.searchBar.updateLastSuggestions(it)
        })
    }
}