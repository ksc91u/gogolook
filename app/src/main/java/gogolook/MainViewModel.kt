package gogolook

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import gogolook.dto.HistoryDto
import gogolook.dto.PixImage
import gogolook.service.PixApi
import gogolook.service.PreferenceKeys
import gogolook.service.PreferenceManager
import gogolook.service.adapter.NetworkResponse
import gogolook.ui.home.ImagePagingSource
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.launch
import timber.log.Timber

class MainViewModel(private val pixApi: PixApi, private val preferenceManager: PreferenceManager) :
    ViewModel() {

    val pageLiveData: MutableLiveData<PagingData<PixImage>> = MutableLiveData()


    private val _isLoading: MutableLiveData<Boolean> = MutableLiveData()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _isError: MutableLiveData<Boolean> = MutableLiveData()
    val isError: LiveData<Boolean> = _isError

    private val _fabIconRes: MutableLiveData<Int> = MutableLiveData(R.drawable.outline_looks_one_24)
    val fabIconRes: LiveData<Int> = _fabIconRes

    val fabClickFlow: MutableSharedFlow<Unit> = MutableSharedFlow()

    val defaultSpanCount: MutableLiveData<Int> = MutableLiveData(1)

    private val historyList: MutableList<String> = mutableListOf()
    val historyListLiveData: MutableLiveData<List<String>> = MutableLiveData(emptyList())

    init {
        viewModelScope.launch {
            val column = pixApi.getColumn()
            when (column) {
                is NetworkResponse.Success -> {
                    defaultSpanCount.value = column.body.configValue.toIntOrNull() ?: 1
                }
                else -> {
                    defaultSpanCount.value = 1
                    Timber.d(">>> Column ${column.cause}")
                }
            }
        }

        viewModelScope.launch {
            val history = preferenceManager.getJsonPref(PreferenceKeys.historyPreferenceKey)
            historyList.clear()
            history.firstOrNull()?.keywords?.let(historyList::addAll)
            historyListLiveData.value = historyList.distinct()
        }
    }

    fun fabClick() {
        viewModelScope.launch {
            fabClickFlow.emit(Unit)
        }
    }


    fun search(keyword: String) {
        viewModelScope.launch {
            val flow = Pager(PagingConfig(20, initialLoadSize = 20)) {
                ImagePagingSource(pixApi, keyword)
            }.flow.cachedIn(viewModelScope)
            flow.collect { pagingData ->
                pageLiveData.value = pagingData
            }
        }
        updateHistoryList(keyword)
    }

    fun updateHistoryList(keyword: String) {
        historyList.add(0, keyword)
        historyListLiveData.value = historyList.distinct()
        viewModelScope.launch {
            preferenceManager.setJsonPref(
                PreferenceKeys.historyPreferenceKey,
                HistoryDto(historyList.distinct())
            )
        }
    }

    fun setLoadState(loadStates: CombinedLoadStates) {

    }

    fun setSpanCount(spanCount: Int) {
        _fabIconRes.value =
            if (spanCount == 1) R.drawable.outline_looks_one_24 else R.drawable.outline_looks_two_24
    }

    fun searchSuggestion(position: Int) {
        search(historyListLiveData.value?.get(position) ?: "")
    }

    fun removeSuggestion(position: Int) {
        val oldList = historyListLiveData.value?.toMutableList() ?: return
        oldList.removeAt(position)
        historyList.clear()
        historyList.addAll(oldList)
        historyListLiveData.value = oldList

    }
}