package gogolook.service

import gogolook.service.adapter.NetworkResponseAdapterFactory
import gogolook.service.di.MoshiSingleton
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

val pixModule = module {

    //okhttp
    single {
        OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .pingInterval(10, TimeUnit.SECONDS)
            .addInterceptor { chain ->
                val original = chain.request()
                val newRequest = if (original.url.host.contains("pixabay.com")) {
                    val newUrl = original.url.newBuilder()
                        .addQueryParameter("key", "23468546-eaaec891cd94c60b6c41f30cf").build()
                    original.newBuilder()
                        .url(newUrl)
                        .build()
                } else {
                    original
                }
                return@addInterceptor chain.proceed(newRequest)
            }.addInterceptor(
                HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY)
            ).build()

    }

    //retrofit
    single {
        Retrofit.Builder()
            .baseUrl("https://pixabay.com/")
            .client(get<OkHttpClient>())
            .addConverterFactory(MoshiConverterFactory.create(get()))
            .addCallAdapterFactory(NetworkResponseAdapterFactory())
            .build()
    }

    //moshi
    single {
        MoshiSingleton.moshiInstance
    }

    //api
    single {
        get<Retrofit>().create(PixApi::class.java)
    }

    //preference
    single {
        PreferenceManager(androidApplication())
    }
}