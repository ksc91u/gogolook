package gogolook.service.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

object MoshiSingleton {
    val moshiInstance = Moshi.Builder()
        .addLast(KotlinJsonAdapterFactory())
        .build()!!
}