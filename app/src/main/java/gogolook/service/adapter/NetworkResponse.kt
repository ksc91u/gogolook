package gogolook.service.adapter

import java.io.IOException

sealed class NetworkResponse<out T : Any, out U : Any> {

    var isErrorHandled = false

    abstract val cause: Throwable?

    /**
     * Success response with body
     */
    data class Success<T : Any>(val body: T) : NetworkResponse<T, Nothing>() {
        override val cause: Throwable?
            get() = null
    }

    /**
     * Failure response with body
     */
    data class ApiError<U : Any>(val body: U, val code: Int) : NetworkResponse<Nothing, U>() {
        override val cause: Throwable
            get() = RuntimeException("[ApiError] code:$code, body:$body")
    }

    /**
     * Network error
     */
    data class NetworkError(val error: IOException) : NetworkResponse<Nothing, Nothing>() {
        override val cause: Throwable
            get() = error
    }

    /**
     * For example, json parsing error
     */
    data class UnknownError(val error: Throwable) : NetworkResponse<Nothing, Nothing>() {
        override val cause: Throwable
            get() = error
    }

    fun <T2 : Any> toAnotherTypedError(): NetworkResponse<T2, U> {
        return when (this) {
            is Success -> error(
                "Usage error, shouldn't invoke `toAnotherError` on a Success instance"
            )
            is ApiError -> ApiError(body, code)
            is NetworkError -> NetworkError(error)
            is UnknownError -> UnknownError(error)
        }
    }

    suspend fun getOrNull(): T? = when (this) {
        is Success -> body
        is ApiError,
        is NetworkError,
        is UnknownError -> {
            null
        }
    }
}

fun Throwable.toUnknownError() = NetworkResponse.UnknownError(this)
