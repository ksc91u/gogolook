package gogolook.service

import android.app.Application
import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.squareup.moshi.JsonAdapter
import gogolook.dto.HistoryDto
import gogolook.service.di.MoshiSingleton
import kotlinx.coroutines.flow.*
import timber.log.Timber
import java.io.IOException


val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "gogolook")

object PreferenceKeys {
    val historyPreferenceKey = JsonPreferenceKey(HistoryDto::class.java)
}

class JsonPreferenceKey<T>(clazz: Class<T>) {
    val stringKey = stringPreferencesKey("$JSON_PREFERENCE_KEY_PREFIX${clazz.canonicalName}")
    val jsonAdapter: JsonAdapter<T> = MoshiSingleton.moshiInstance.adapter(clazz)

    companion object {
        const val JSON_PREFERENCE_KEY_PREFIX = "JSON_"
    }
}


class PreferenceManager(val context: Application) {
    private val dataStore = context.dataStore

    suspend fun <J> setJsonPref(key: JsonPreferenceKey<J>, value: J) =
        setPref(key.stringKey, key.jsonAdapter.toJson(value))

    suspend fun <T> setPref(key: Preferences.Key<T>, value: T) {

        dataStore.edit {
            it[key] = value
        }
    }

    suspend fun <T> getPref(key: Preferences.Key<T>): Flow<T?> {
        return dataStore.data.catch { exception ->
            // dataStore.data throws an IOException when an error is encountered when reading data
            Timber.d("$exception")
            if (exception is IOException) {
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }.map {
            it[key]
        } ?: emptyFlow()
    }

    suspend fun <J> getJsonPref(key: JsonPreferenceKey<J>): Flow<J?> =
        getPref(key.stringKey).filterNotNull().map {
            key.jsonAdapter.fromJson(it)
        }
}