package gogolook.service

import gogolook.dto.ConfigDto
import gogolook.dto.PixbayResult
import gogolook.service.adapter.NetworkResponse
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface PixApi {
    @GET("api/")
    suspend fun search(
        @Query("q") q: String,
        @Query("page") page: Long
    ): NetworkResponse<PixbayResult, Unit>

    @GET
    suspend fun getColumn(@Url url: String = "http://oracle-jp-1.ksc91u.info:8080/config/column"): NetworkResponse<ConfigDto, Unit>

}