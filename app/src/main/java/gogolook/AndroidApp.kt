package gogolook

import android.app.Application
import gogolook.service.di.viewModelModule
import gogolook.service.pixModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class AndroidApp : Application() {
    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())
        startKoin{
            androidContext(this@AndroidApp)
            modules(
                listOf(pixModule, viewModelModule)
            )
        }

    }
}